//---------------------------------------------------
// Include dependencies
//---------------------------------------------------
var gulp 		 = require('gulp'),
	sass 		 = require('gulp-sass'),
	concat 		 = require('gulp-concat'),
	watch  		 = require('gulp-watch'),
	plumber 	 = require('gulp-plumber'),
	minifyCss 	 = require('gulp-minify-css'),
	uglify    	 = require('gulp-uglify'),
	notify		 = require('gulp-notify'),
	sourcemaps   = require('gulp-sourcemaps'),
	prefixer 	 = require('autoprefixer');

//---------------------------------------------------
var distJS  = 'dist/js';
var distCSS = 'dist/css';
var srcSASS = 'src/sass/**/*.scss';
var srcJS   = 'src/js/**/*.js';

//------------------------------------------

// SASS to CSS
gulp.task('styles', function(){
	return gulp.src(srcSASS)
		.pipe(plumber())
		.pipe(sass())
		//.pipe(prefixer('last 2 version'))
		.pipe(concat('app.min.css'))
		.pipe(gulp.dest(distCSS))
		.pipe(minifyCss())
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(distCSS))
		.pipe(notify({message: 'We are done builing CSS!!'}));
});

//---------------------------------------------------

//Compile JS
gulp.task('scripts', function(){
	return gulp.src(srcJS)
		.pipe(plumber())
		.pipe(uglify())
		.pipe(concat('app.min.js'))
		.pipe(sourcemaps.init())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(distJS))
		.pipe(notify({message: 'We are done builing JS!!'}));
});

//---------------------------------------------------
gulp.task('watch', function(){
	gulp.watch(srcJS, ['scrips']);
	gulp.watch(srcSASS,['styles']);
});